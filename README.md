# cat_vs_dog
В данном проекте реализовано клиент-сервер взаимодействие на примере классификации объектов изображения (для простоты рассматривается всего два класса: изображен кот или нет). Действительно, может быть полезна данная реализация, так как из-за размеров изображения определение принадлежности классу может занимать продолжительное время, и, как следствие, последовательная обработка очереди картинок.
## Установка
Проект состоит из следующих файлов: [client](https://bitbucket.org/dakovalev1/cat_vs_dog/src/master/client.py), 
[server](https://bitbucket.org/dakovalev1/cat_vs_dog/src/master/server.py), [utils](https://bitbucket.org/dakovalev1/cat_vs_dog/src/master/utils.py), [current_model](https://bitbucket.org/dakovalev1/cat_vs_dog/src/master/current_model.h5).
## Запуск
Сервер в качестве аргументов командной строки принимает обученную модель -- '--model', порт -- '--port', ссылки на источники изображений через запятую -- '--urls' и число клиентов -- '--workers', которое по дефолту считается равным 1.

Клиент в качестве аргументов командной строки принимает адрес серсера и его порт, соответственно переменные '--addr', '--port'.

По мере запуска выводятся этапы работы приложения-сервера:
```
Wait for all workers
Start sending tasks
Download cat image #N from source
...
```
А также приложения-клиента:
```
connected
model size: ...
model recieved
wait task
new task
Result: img_url is cat or dog URL
Connection is closed, tear down
```
Результат работы клиента отправляется серверу в виде метки класса, по которым сервер скачивает изображения только котиков.
